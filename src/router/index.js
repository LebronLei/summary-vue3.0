import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
    history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
    routes: [
        {
            path: '/',
            redirect: '/summary'
        },
        {
            path: '/home',
            name: 'home',
            component: () => import('@/views/Home.vue'),
            meta: {
                index: 1
            }
        },
        {
            path: '/summary',
            name: 'summary',
            component: () => import('@/views/summary/index.vue'),
            meta: {
                index: 2
            }
        },

    ]
})

export default router